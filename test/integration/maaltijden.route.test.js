const chai = require('chai')
var should = require('chai').should();
const chaiHttp = require('chai-http')
const server = require('../../server')
const tracer = require('tracer')
const maaltijdenController = require('../../src/controllers/maaltijden.controller')
const studenthuiscontroller = require('../../src/controllers/studenthuis.controller')

chai.should()
chai.use(chaiHttp)
tracer.setLevel('error')

// studenthuiscontroller.database = [{ 
//     "homeId": "1",
//     "naam": "Hans",
//     "straatnaam": "Spruytstraat ",
//     "huisnummer": 4,
//     "postcode": "3081 FK",
//     "plaats": "test",
//     "telefoonnummer": "0657483906"
// }, { 
//     "homeId": "5",
//     "naam": "Klaas",
//     "straatnaam": "Spruytstraat ",
//     "huisnummer": 4,
//     "postcode": "3081 FK",
//     "plaats": "test",
//     "telefoonnummer": "0657483906"
// }]



// describe('Manage studenthomes', () => {
//   describe('UC-301 Maaltijd aanmaken', () => {
//     beforeEach((done) => {
//     maaltijdenController.meals=[]
//     done()
//     })

//     it('UC-301 Maaltijd aanmaken succesvol' , (done) => {
//       // server starten
//       // POST versturen, onvolledige info meesturen
//       // Valideren dat verwachte waarden kloppen

//       chai
//         .request(server)
//         .post('/studenthome/1/meal')
//         .send({
            
//                 homeId: "1",
//                 mealId: "50",
//                 naam: "lasagna",
//                 beschrijving: "italiaanse lasagna",
//                 aangemaakt: "03/02/2019, 22:00",
//                 aangeboden: "3-/07/2020, 22:00",
//                 prijs: "8,00",
//                 allergieInformatie: "Pindas", 
//                 ingredienten: "tomaar, mozarella, pastadeeg"
            
//         })
//         .end((err, res) => {
//           res.should.have.status(200)
//           res.should.be.an('object')
//           let { result } = res.body
//           result.should.be.an('array')
//           done()
//         })
//     })
//     it('UC-301 Maaltijd aanmaken verkeerd homeId' , (done) => {
//         // server starten
//         // POST versturen, onvolledige info meesturen
//         // Valideren dat verwachte waarden kloppen
  
//         chai
//           .request(server)
//           .post('/studenthome/-1/meal')
//           .send({
              
//                   homeId: "1",
//                   mealId: "50",
                  
//                   beschrijving: "italiaanse lasagna",
//                   aangemaakt: "03/02/2019, 22:00",
//                   aangeboden: "3-/07/2020, 22:00",
//                   prijs: "8,00",
//                   allergieInformatie: "Pindas", 
//                   ingredienten: "tomaar, mozarella, pastadeeg"
              
//           })
//           .end((err, res) => {
//               console.log(res.body)
//             res.should.have.status(400)
//             res.should.be.an('object')
//             let { message } = res.body
//             message.should.be.an('string').that.equals("homeId is incorrect")
//             done()
//           })
//       })
//       it('UC-301 Verplicht veld ontbreekt' , (done) => {
//         // server starten
//         // POST versturen, onvolledige info meesturen
//         // Valideren dat verwachte waarden kloppen
  
//         chai
//           .request(server)
//           .post('/studenthome/1/meal')
//           .send({
              
//                   homeId: "1",
//                   mealId: "50",
//                   naam: "lasagna",
//                   beschrijving: "italiaanse lasagna",
//                   aangemaakt: "03/02/2019, 22:00",
//                   aangeboden: "3-/07/2020, 22:00",
//                   prijs: "8,00",
//                   allergieInformatie: "Pindas", 
//                   ingredienten: "tomaar, mozarella, pastadeeg"
              
//           })
//           .end((err, res) => {
//               console.log(res.body)
//             res.should.have.status(400)
//             res.should.be.an('object')
//             let { message, error } = res.body
//             message.should.be.an('string').that.equals('Error message')
//             error.should.be.an('string').that.equals('AssertionError [ERR_ASSERTION]: naam is niet ingevuld')
//             done()
//           })
//       })
// })
// })