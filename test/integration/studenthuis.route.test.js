// process.env.DB_DATABASE = 'testing'

const chai = require('chai')
var should = require('chai').should()
const chaiHttp = require('chai-http')
const server = require('../../server')
const tracer = require('tracer')
const maaltijdenController = require('../../src/controllers/maaltijden.controller')
const studenthuiscontroller = require('../../src/controllers/studenthuis.controller')
const connection = require('../../src/database/connection')

chai.should()
chai.use(chaiHttp)
tracer.setLevel('error')
const jwt = require('jsonwebtoken')

// jwt.sign({id: 1}, process.env.KEY, {expiresIn: '2h' }, (err, token)=>{})
let usertokenID

describe('Manage studenthomes', () => {
  //let createStudentid
  let deleteid
  describe('UC-201 Create studenthome - POST api/studenthome', () => {
    beforeEach((done) => {
      connection.query(
        `DELETE FROM studenthome WHERE Name = "Jan" AND Address = "Spruytstraat" AND city = "test"`,
        (err, rows, fields) => {
          if (err) {
            done(err)
          } else {
            done()
          }
        }
      )
      // done()
    })

    it('TC-201-1 should return valid error when required value is not present', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              straatnaam: 'Spruytstraat ',
              huisnummer: 4,
              postcode: '3081 FK',
              plaats: 'test',
              telefoonnummer: '0657483906'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { message, error } = res.body

              message.should.be.an('string').that.equals('foute input')
              error.should.be
                .an('string')
                .that.equals('AssertionError [ERR_ASSERTION]: naam is nodig')

              done()
            })
        }
      )
    })

    it('TC-201-2 should return a valid error when postal code is invalid. ', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              naam: 'Jan',
              straatnaam: 'Spruytstraat ',
              huisnummer: 4,
              postcode: '3081',
              plaats: 'test',
              telefoonnummer: '0657483906'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { message, error } = res.body
              message.should.be.an('string').that.equals('foute input')
              error.should.be
                .an('string')
                .that.equals('AssertionError [ERR_ASSERTION]: foute postcode')

              done()
            })
        }
      )
    })

    it('TC-201-3 should return a valid error when telephone number code is invalid. ', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              homeId: '1',
              naam: 'Jan',
              straatnaam: 'Spruytstraat ',
              huisnummer: 4,
              postcode: '3081 DD',
              plaats: 'test',
              telefoonnummer: '06576'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { message, error } = res.body
              message.should.be.an('string').that.equals('foute input')
              error.should.be
                .an('string')
                .that.equals(
                  'AssertionError [ERR_ASSERTION]: foute telefoonnummer'
                )

              done()
            })
        }
      )
    })

    it('TC-201-4 Studentenhuis bestaat al', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              naam: 'Lovensdijk',
              straatnaam: 'Lovensdijkstraat',
              huisnummer: 62,
              postcode: '4706 RX',
              telefoonnummer: '06123456789',
              plaats: 'Breda'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { result } = res.body

              result.should.be
                .an('string')
                .that.equals('Studenthome bestaat al')

              done()
            })
        }
      )
    })

    it('TC-201-5 Niet ingelogd', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + "d")
            .send({
              naam: 'Lovensdijk',
              straatnaam: 'Lovensdijkstraat',
              huisnummer: 62,
              postcode: '4706 RX',
              telefoonnummer: '06123456789',
              plaats: 'Breda'
            })
            .end((err, res) => {
              res.should.have.status(401)
              res.should.be.an('object')
              let { error, datetime } = res.body

              error.should.be
                .an('string')
                .that.equals('Not authorized')

              done()
            })
          })
    })

    it('TC-201-6 Studentenhuis  succesvol toegevoegd ', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              naam: 'Jan',
              straatnaam: 'Spruytstraat ',
              huisnummer: 4,
              postcode: '3081 DD',
              plaats: 'test',
              telefoonnummer: '0123456789'
            })
            .end((err, res) => {
              res.should.have.status(200)
              res.should.be.an('object')
              let { result } = res.body

              createStudentid = result.Id
              

              result.result.should.be.an('object')

              done()
            })
        }
      )
    })
  })
  describe('UC-202 Overzichten studenthomes - GET api/studenthome', () => {
    beforeEach((done) => {
      done()
    })

    it('TC-202-1 Toon nul studentenhuizen', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen

      chai

        .request(server)
        .get('/api/studenthome?name=-1')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')
          result.result.length.should.equal(0)

          done()
        })
    })


    it('TC-202-2 show 2 student homes', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai
        .request(server)
        .get('/api/studenthome?city=rdam')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body

          result.result.length.should.equal(2)

          done()
        })
    })

    it('TC-202-3 Toon studentenhuizen met zoekterm op niet-bestaande naam', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?name=-1')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')
          result.result.length.should.equal(0)

          done()
        })
    })

    it('TC-202-4 Toon studentenhuizen met zoekterm op niet-bestaande stad', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?city=g')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')
          result.result.length.should.equal(0)

          done()
        })
    })
    it('TC-202-5 TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?city=Breda')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')

          done()
        })
    })

    it('TC-202-6 Toon studentenhuizen met zoekterm op bestaande naam', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?name=Jan')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')

          done()
        })
    })
  })
  describe('UC-203 Details van studentenhuis', () => {
    beforeEach((done) => {
      done()
    })
    it('TC-203-1 Studentenhuis-ID bestaat niet', (done) => {
      
          chai
            .request(server)
            .get('/api/studenthome/-1')
            .end((err, res) => {
              res.should.have.status(404)
              res.should.be.an('object')
              const {result} = res.body
              result.should.be
                .an('string')
                .that.equals("Studenthome does not exist")
              
              done()
            })
        
      
    })
    it('TC-203-2 Studentenhuis-ID bestaat', (done) => {
      
          chai
            .request(server)
            .get('/api/studenthome/4') 
            .end((err, res) => {
              res.should.have.status(200)
              res.should.be.an('object')
              const { result } = res.body
              result.result[0].ID.should.be
                .an('number')
                .that.equals(4)
              done()
            })
    })
  })
  describe('UC-204 Studentenhuis wijzigen', () => {
    beforeEach((done) => {
      done()
    })
    it('TC-204-1 Verplicht veld ontbreekt', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .put('/api/studenthome/1')
            .set('authorization', 'Bearer ' + token)
            .send({
              straatnaam: 's',
              huisnummer: 22,
              postcode: '1111 LL',
              plaats: 'a',
              telefoonnummer: '0123456789'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              const { message, error } = res.body
              message.should.be
                .an('string')
                .that.equals('verplicht veld niet ingevuld')
              error.should.be.an('string')
              done()
            })
        }
      )
    })
    it('TC-204-2 Invalide postcode', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .put('/api/studenthome/1')
            .set('authorization', 'Bearer ' + token)
            .send({
              id: 50,
              naam: 's',
              straatnaam: 's',
              huisnummer: 22,
              postcode: '1111 L',
              plaats: 'a',
              telefoonnummer: '0123456789'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              const { message, error } = res.body
              message.should.be
                .an('string')
                .that.equals('verplicht veld niet ingevuld')
              error.should.be
                .an('string')
                .that.equals('AssertionError [ERR_ASSERTION]: foute postcode')
              done()
            })
        }
      )
    })
    it('TC-204-3 Invalid telefoonnummer', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
          chai
            .request(server)
            .put('/api/studenthome/1')
            .set('authorization', 'Bearer ' + token)
            .send({
              id: 50,
              naam: 's',
              straatnaam: 's',
              huisnummer: 22,
              postcode: '1111 LF',
              plaats: 'a',
              telefoonnummer: '056'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              const { message, error } = res.body
              message.should.be
                .an('string')
                .that.equals('verplicht veld niet ingevuld')
              error.should.be
                .an('string')
                .that.equals(
                  'AssertionError [ERR_ASSERTION]: foute telefoonnummer'
                )
              done()
            })
        }
      )
    })
    it('TC-204-4 Studentenhuis bestaat niet', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .put('/api/studenthome/-1')
            .set('authorization', 'Bearer ' + token)
            .send({
              id: 50,
              naam: 's',
              straatnaam: 's',
              huisnummer: 22,
              postcode: '1111 LF',
              plaats: 'a',
              telefoonnummer: '0560000000'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              const { message, error } = res.body
              message.should.be
                .an('string')
                .that.equals('verplicht veld niet ingevuld')
              error.should.be
                .an('string')
                .that.equals('ReferenceError: e is not defined')
              done()
            })
        }
      )
    })
    it('TC-204-5 Niet ingelogd', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .put('/api/studenthome/-1')
            .set('authorization', 'Bearer ' + "g")
            .send({
              id: 50,
              naam: 's',
              straatnaam: 's',
              huisnummer: 22,
              postcode: '1111 LF',
              plaats: 'a',
              telefoonnummer: '0560000000'
            })
            .end((err, res) => {
              res.should.have.status(401)
              res.should.be.an('object')
              const {  error } = res.body
              
              error.should.be
                .an('string')
                .that.equals('Not authorized')
              done()
            })
          })
    })
    it('TC-204-6 Studentenhuis  succesvol gewijzigd', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .put('/api/studenthome/1')
            .set('authorization', 'Bearer ' + token)
            .send({
              id: 50,
              naam: 's',
              straatnaam: 's',
              huisnummer: 22,
              postcode: '1111 LF',
              plaats: 'a',
              telefoonnummer: '0560000000'
            })
            .end((err, res) => {
              res.should.have.status(200)
              res.should.be.an('object')
              const { result } = res.body
              result.should.be.an('object')
              done()
            })
        }
      )
    })
  })
  describe('delete', () => {
    beforeEach((done) => {
      connection.query(
        `SELECT ID FROM studenthome WHERE Name = "Jan"`,
        (err, rows, fields) => {
          if (err) {
            done(err)
          } else {
            deleteid = rows
            done()
          }
        }
      )
      // done()
    })
    it('TC-205-1 Studentenhuis bestaat niet', (done) => {
      
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .delete('/api/studenthome/-1')
            .set('authorization', 'Bearer ' + token)
            .end((err, res) => {
              res.should.have.status(404)
              res.should.be.an('object')
              const { result } = res.body
              result.should.be
                .an('string')
                .that.equals('Studenthome does not exist')
              done()
            })
        }
      )
    })
    it('TC-205-2 Niet ingelogd', (done) => {
      
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .delete('/api/studenthome/1')
            .set('authorization', 'Bearer ' + "s")
            .end((err, res) => {
              res.should.have.status(401)
              res.should.be.an('object')
              const { error } = res.body
              error.should.be
                .an('string')
                .that.equals('Not authorized')
              done()
            })
        }
      )
    })

    it('TC-205-3Actor is geen eigenaar', (done) => {
      
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .delete('/api/studenthome/1')
            .set('authorization', 'Bearer ' + "s")
            .end((err, res) => {
              res.should.have.status(401)
              res.should.be.an('object')
              const { error } = res.body
              error.should.be
                .an('string')
                .that.equals('Not authorized')
              done()
            })
        }
      )
    })

    it('TC-205-4 Studentenhuis  succesvol verwijderd', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.TOKEN_KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .delete(`/api/studenthome/${deleteid[0].ID}`)
            .set('authorization', 'Bearer ' + token)
            .end((err, res) => {
              res.should.have.status(200)
              res.should.be.an('object')
              const { result } = res.body

              result[0].ID.should.be.an('number').that.equals(deleteid[0].ID)
              // result.result.result[0].ID.should.be.an('string').equals(createStudentid)
              //result.result.should.be.an('string').equals("Studenthome is removed")

              done()
            })
        }
      )
    })
  })
})
