var mysql = require('mysql')
const connection = require('./connection')
const log = require('../config/config')
const logger = log.logger

//process.env.DB_DATABASE = process.env.DB_DATABASE || 'studenthome'

let database = {
  createStudenthome(studenthomeJson, userId, callback) {
    const {
      naam,
      straatnaam,
      huisnummer,
      postcode,
      plaats,
      telefoonnummer
    } = studenthomeJson

    connection.query(
      `INSERT INTO studenthome (Name,Address,House_Nr,UserID,Postal_Code,Telephone,City) 
          VALUES('${naam}','${straatnaam}',${huisnummer},${userId},'${postcode}','${telefoonnummer}','${plaats}')`,
      function (error, results, fields) {
        if (error) {
          callback(undefined, {
            error: 'Studenthome bestaat al'
          })
        } else {
          callback(undefined, {
            Id: results.insertId,
            result: studenthomeJson
          })
        }

        // connection.end()
      }
    )
  },

  

  addMeal(meal, homeIdparam, userid, callback) {
    const {
      naam,
      beschrijving,
      ingredienten,
      aangemaakt,
      aangeboden,
      prijs,
      allergieInformatie,
      maxParticipants
    } = meal

    connection.query(
      `INSERT IGNORE INTO meal(Name,Description,Ingredients,Allergies,CreatedOn,OfferedOn,Price,UserID,StudenthomeID,MaxParticipants) VALUES('${naam}','${beschrijving}','${ingredienten}','${allergieInformatie}','${aangemaakt}','${aangeboden}',${prijs},${userid},${homeIdparam},${maxParticipants})`,
      function (error, results, fields) {
        if (error) throw error

        if (error || results.affectedRows === 0) {
          callback(undefined, {
            result: 'Studenthome does not exist'
          })
        } else {
          callback(undefined, {
            Id: results.insertId,
            result: meal
          })
        }
        //logger.debug('The solution is: ', results);
        // connection.end()
      }
    )
  },

  getAllMeals(homeId, callback) {
    connection.query(
      `SELECT * FROM meal WHERE StudenthomeId = ${homeId}`,
      function (error, results, fields) {
        if (error) throw error
        //logger.debug('The solution is: ', results);
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },
  updateMeals(updatedMeal, homeparam, mealparam, userid, callback) {
    const {
      mealId,
      naam,
      beschrijving,
      ingredienten,
      allergieinformatie,
      aangemaaktOp,
      aangeboden,
      prijs,
      maxParticipants
    } = updatedMeal

    connection.query(
      `UPDATE meal SET Name = '${naam}', Description = '${beschrijving}', Ingredients = '${ingredienten}', Allergies = '${allergieinformatie}', CreatedOn = '${aangemaaktOp}', OfferedOn = '${aangeboden}', Price = ${prijs}, StudenthomeID = '${homeparam}', MaxParticipants = ${maxParticipants} WHERE StudentHomeID = ${homeparam} AND ID = ${mealparam} AND UserID = ${userid}`,
      function (error, results, fields) {
        if (error) throw error
        //logger.debug('The solution is: ', results);
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },

  deleteMeal(homeIdparam, mealIdparam, userid, callback) {
    let meal

    this.getMealById(homeIdparam, mealIdparam, (err, result) => {
      if (err) {
        meal = 'meal does not exist'
      } else {
        meal = result
      }
    })

    connection.query(
      `DELETE FROM meal WHERE StudenthomeID = ${homeIdparam} AND ID = ${mealIdparam} AND UserID = ${userid}`,
      function (error, results, fields) {
        if (error) throw error
        logger.debug('affected rows = ', results.affectedRows)
        if (results.affectedRows === 0) {
          callback(undefined, {
            result: 'meal does not exist'
          })
        } else {
          callback(undefined, {
            meal
          })
        }
        // connection.end()
      }
    )
  },
  getMealById(homeIdparam, mealIdparam, callback) {
    connection.query(
      `SELECT * FROM meal WHERE StudenthomeID = ${homeIdparam} AND ID = ${mealIdparam}`,
      function (error, results, fields) {
        if (error) throw error

        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },
  getStudenthomesEmptyFields(callback) {
    connection.query('SELECT * FROM studenthome', function (
      error,
      results,
      fields
    ) {
      if (error) throw error

      callback(undefined, {
        result: results
      })
      // connection.end()
    })
  },
  getStudenthomesByCity(plaats, callback) {
    connection.query(
      `SELECT * FROM studenthome WHERE City='${plaats}'`,
      function (error, results, fields) {
        if (error) throw error
        //logger.debug('The solution is: ');
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },
  getStudenthomesByName(naam, callback) {
    connection.query(
      `SELECT * FROM studenthome WHERE Name='${naam}'`,
      function (error, results, fields) {
        if (error) throw error
        //logger.debug('The solution is: ');
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },
  getStudenthomesByNameAndCity(naam, plaats, callback) {
    connection.query(
      `select * from studenthome WHERE City='${plaats}' AND Name='${naam}'`,
      function (error, results, fields) {
        if (error) throw error
        //logger.debug('The solution is: ', results);
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },
  getStudentHomeById(Id, callback) {
    connection.query(`SELECT * FROM studenthome WHERE ID=${Id}`, function (
      error,
      results,
      fields
    ) {
      if (error || results.length === 0) {
        callback(undefined, {
          result: 'Studenthome does not exist'
        })
      } else {
        callback(undefined, {
          result: results
        })
      }
      //logger.debug('The solution is: ', results);

      // connection.end()
    })
  },
  changeStudentHome(homeIdParam, studenthome, userid, callback) {
    const {
      naam,
      straatnaam,
      huisnummer,
      postcode,
      plaats,
      telefoonnummer
    } = studenthome

    connection.query(
      `UPDATE studenthome SET ID = ${homeIdParam}, Name='${naam}',Address='${straatnaam}',House_Nr=${huisnummer},Postal_Code='${postcode}',Telephone='${telefoonnummer}',City='${plaats}' WHERE ID=${homeIdParam} AND UserID = ${userid}`,
      function (error, results, fields) {
        if (error) throw error
        logger.debug('affected rows = ', results.affectedRows)
        if (results.affectedRows === 0) {
          callback(undefined, {
            result: 'studenthuis bestaat niet'
          })
        } else {
          callback(undefined, {
            result: studenthome
          })
        }

        // connection.end()
      }
    )
  },

  getUserById(email, callback) {
    connection.query(`SELECT ID FROM user WHERE Email = ${email}`, function (
      error,
      results,
      fields
    ) {
      if (error) throw error

      callback(undefined, {
        result: results
      })
      // connection.end()
    })
  },
  deleteStudentHome(homeIdParam, userid, callback) {
    let studenthome

    this.getStudentHomeById(homeIdParam, (err, result) => {
      if (err) {
        studenthome = err
      } else {
        studenthome = result
      }
    })

    logger.debug('This is in studenthome now = ', studenthome)
    connection.query(
      `DELETE FROM studenthome WHERE ID=${homeIdParam} AND UserID = ${userid}`,
      function (error, results, fields) {
        if (error) throw error
        //logger.debug('The solution is: ', results);

        if (results.affectedRows === 0) {
          callback(undefined, {
            result: 'Studenthome does not exist'
          })
        } else {
          callback(undefined, {
            studenthome
          })
        }
        // connection.end()
      }
    )
  },

  getAllParticipants(mealId, callback) {
    connection.query(
      `SELECT * FROM participants WHERE MealID = ${mealId}`,
      function (error, results, fields) {
        if (error) throw error
        logger.debug('The solution is: ', results)
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },
  signUpForMeal(homeIdParam, mealId, callback) {
    //var d = new Date();

    connection.query(
      `INSERT INTO participants(UserID, StudenthomeID, MealID, SignedUpOn) VALUES (${1},${homeIdParam},${mealId},NOW())`,
      function (error, results, fields) {
        if (error) throw error
        logger.debug('The solution is: ', results)
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },

  signOffForMeal(homeIdParam, mealId, callback) {
    connection.query(
      `DELETE FROM participants WHERE StudenthomeID = ${homeIdParam} AND MealID = ${mealId}`,
      function (error, results, fields) {
        if (error) throw error
        logger.debug('The solution is: ', results)
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },

  getParticipantById(mealId, participantId, callback) {
    connection.query(
      `SELECT * FROM participants WHERE MealID = ${mealId} AND UserID = ${participantId}`,
      function (error, results, fields) {
        if (error) throw error
        logger.debug('The solution is: ', results)
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },

  //voor testing
  insertAllDataIntoStudenthome(callback) {
    connection.query(
      'INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES ' +
        "('Princenhage', 'Princenhage', 11, 1,'4706RX','061234567891','Breda')," +
        "('Haagdijk 23', 'Haagdijk', 4, 1, '4706RX','061234567891','Breda')," +
        "('Den Hout', 'Lovensdijkstraat', 61, 1, '4706RX','061234567891','Den Hout')," +
        "('Den Dijk', 'Langendijk', 63, 1, '4706RX','061234567891','Breda')," +
        "('Lovensdijk', 'Lovensdijkstraat', 62, 1, '4706RX','061234567891','Breda')," +
        "('Van Schravensteijn', 'Schravensteijnseweg', 23, 1, '4706RX','061234567891','Breda')",
      function (error, results, fields) {
        if (error) throw error
        logger.debug('The solution is: ', results)
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },

  insertAllDataIntoMeals(callback) {
    connection.query(
      'INSERT INTO `meal` (Name, Description, Ingredients, Allergies, CreatedOn, OfferedOn, Price, UserID, StudenthomeID) VALUES ' +
        "('Zuurkool met worst', 'Zuurkool a la Montizaan, specialiteit van het huis.', 'Zuurkool, worst, spekjes', 'Lactose, gluten','2020-09-01','2020-09-01', 5, 1, 1)," +
        "('Spaghetti', 'Spaghetti Bolognese', 'Pasta, tomatensaus, gehakt', 'Lactose','2020-09-01','2020-09-01', 3, 1, 1);",
      function (error, results, fields) {
        if (error) throw error
        logger.debug('The solution is: ', results)
        callback(undefined, {
          result: results
        })
        // connection.end()
      }
    )
  },
  removeValuesFromMealTable(callback) {
    connection.query(`delete from meal;`, function (error, results, fields) {
      if (error) throw error
      logger.debug('The solution is: ', results)
      callback(undefined, {
        result: results
      })
      // connection.end()
    })
  },

  removeValuesFromStudenthomesTable(callback) {
    connection.query(`delete from studenthome;`, function (
      error,
      results,
      fields
    ) {
      if (error) throw error
      logger.debug('The solution is: ', results)
      callback(undefined, {
        result: results
      })
      // connection.end()
    })
  },

  removeValuesFromParticipantsTable(callback) {
    connection.query(`delete from participants;`, function (
      error,
      results,
      fields
    ) {
      if (error) throw error
      logger.debug('The solution is: ', results)
      callback(undefined, {
        result: results
      })
      // connection.end()
    })
  },

  removeRegister(email, callback) {
    connection.query(`DELETE FROM User WHERE Email = '${email}'`, function (
      error,
      results,
      fields
    ) {
      if (error) throw error

      callback(undefined, {
        result: results
      })
      // connection.end()
    })
  }
}

module.exports = database
