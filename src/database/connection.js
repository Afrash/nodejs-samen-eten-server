var mysql = require('mysql')
const log = require('../config/config')
const logger = log.logger
require('dotenv').config();

// if (process.env.DB_DATABASE === 'testing') {
//   var connection = mysql.createConnection({
//     host: 'mysql',
//     user: 'root',
//     password: KEY,
//     database: 'testing'
//   })
// } else {
//   var connection = mysql.createConnection({
//     host: '188.166.109.108',
//     user: 'Afrash_Ramjit',
//     password: PRODUCTION_DB_PASSWORD,
//     database: '2151957'
//   })
// }
// if (process.env.DB_DATABASE === 'testing') {
// var connection = mysql.createConnection({
//   host: 'localhost',
//   user: 'root',
//   password: process.env.LOCAL_DB_PASSWORD,
//   database: 'testing'
// })
//   } else {
  var connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
  })
// }

// var connection = mysql.createConnection({
//   host: process.env.DB_HOST,
//   user: process.env.DB_USER,
//   database: process.env.DB_DATABASE,
//   password: ''
  
// })



connection.connect(function (err) {
  if (err) throw err
  logger.info('connected as id ' + connection.threadId)
})




module.exports = connection
