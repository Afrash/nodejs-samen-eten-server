const express = require('express')
const router = express.Router()
const studenthuiscontroller = require('../controllers/studenthuis.controller')
const infocontroller = require('../controllers/info.controller')
const authcontroller = require('../controllers/authentication.controller')
const log = require('../config/config')
const logger = log.logger

//studenthuis routes

//UC-103, informatie over server
router.get('/info', infocontroller.getInfo)

//UC-201, maak studentenhuis aan
router.post(
  '/studenthome',
  authcontroller.validateToken,
  studenthuiscontroller.createStudenthuis
)

//UC-202, geeft lijst van alle studenthuizen.
router.get('/studenthome', studenthuiscontroller.getAll)

//UC-203, geeft details van studenthuizen met homeId
router.get('/studenthome/:homeId', studenthuiscontroller.getById)

//UC-204, wijzigen van studenthuis
router.put(
  '/studenthome/:homeId',
  authcontroller.validateToken,
  studenthuiscontroller.changeById
)

//UC-205, verwijderen van studenthuis
router.delete(
  '/studenthome/:homeId',
  authcontroller.validateToken,
  studenthuiscontroller.deleteById
)

module.exports = router
