const express = require('express')
const router = express.Router()
const deelnemercontroller = require('../controllers/deelnemer.controller')

//UC-401 Aanmelden voor maaltijd
router.post(
  '/studenthome/:homeId/meal/:mealId/signup',
  deelnemercontroller.signupMeal
)

//UC-402 Afmelden voor maaltijd
router.put(
  '/studenthome/:homeId/meal/:mealId/signoff',
  deelnemercontroller.signoffMeal
)

//UC-403 Lijst van deelnemers opvragen
router.get(
  '/meal/:mealId/participants',
  deelnemercontroller.getListParticipants
)

//UC-404Details van deelnemer opvragen
router.get(
  '/meal/:mealId/participants/:participantId',
  deelnemercontroller.getParticipantsById
)

module.exports = router
