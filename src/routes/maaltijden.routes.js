const express = require('express')
const router = express.Router()
const maaltijdencontroller = require('../controllers/maaltijden.controller')
const authcontroller = require('../controllers/authentication.controller')

//UC-301, maaltijden aanmaken
router.post(
  '/studenthome/:homeId/meal',
  authcontroller.validateToken,
  maaltijdencontroller.createMeal
)

//UC-302, maaltijden wijzigen
router.put(
  '/studenthome/:homeId/meal/:mealId',
  authcontroller.validateToken,
  maaltijdencontroller.updateMeal
)

// //UC-303, lijst van maaltijden opvragen
router.get('/studenthome/:homeId/meal', maaltijdencontroller.allMeals)

// //UC-304, Details van een maaltijd opvragen
router.get('/studenthome/:homeId/meal/:mealId', maaltijdencontroller.mealById)

// //UC-305, maaltijd verwijderen
router.delete(
  '/studenthome/:homeId/meal/:mealId',
  authcontroller.validateToken,
  maaltijdencontroller.deleteMeal
)

module.exports = router
