const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const sqlDatabase = require('../database/database')


let database = []



let controller = {
  
  createStudenthuis(req, res, next) {
    logger.info('Post aangeroepen op /api/studenthome')

    try {
      const {
        naam,
        straatnaam,
        huisnummer,
        postcode,
        plaats,
        telefoonnummer
      } = req.body

      assert(typeof naam === 'string', 'naam is nodig')
      assert(typeof straatnaam === 'string', 'straatnaam is nodig')
      assert(typeof huisnummer === 'number', 'huisnummer is nodig')
      assert(typeof postcode === 'string', 'postcode is nodig')
      assert.match(
        postcode,
        /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i,
        'foute postcode'
      )
      assert(typeof plaats === 'string', 'plaats is nodig')
      assert(typeof telefoonnummer === 'string', 'telefoonnummer is nodig')
      assert.match(
        telefoonnummer,
        /(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/,
        'foute telefoonnummer'
      )

      sqlDatabase.createStudenthome(req.body, req.userId, (err, result) => {
        if (err || result.error === 'Studenthome bestaat al') {
          res.status(400).json({
            result: 'Studenthome bestaat al'
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    } catch (e) {
      res.status(400).json({
        message: 'foute input',
        error: e.toString()
      })
    }
  },

  getAll(req, res, next) {
    logger.info('Get aangeroepen op /api/studenthome?name=:name&city=:city')

    const name = req.query.name
    const city = req.query.city

    //name and city is undifined
    if (name === undefined && city === undefined) {
      logger.info('name and city are both undifined ')
      sqlDatabase.getStudenthomesEmptyFields((err, result) => {
        if (err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    } else if (name === undefined && city !== undefined) {
      logger.info('city is defined')
      sqlDatabase.getStudenthomesByCity(req.query.city, (err, result) => {
        if (err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    } else if (city === undefined && name !== undefined) {
      logger.info('name is defined')
      sqlDatabase.getStudenthomesByName(req.query.name, (err, result) => {
        if (err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    } else if (city !== undefined && name !== undefined) {
      sqlDatabase.getStudenthomesByNameAndCity(
        req.query.name,
        req.query.city,
        (err, result) => {
          if (err) {
            res.status(400).json({
              result: err
            })
          } else {
            res.status(200).json({
              result: result
            })
          }
        }
      )
    }
  },

  getById(req, res, next) {
    logger.info('Get aangeroepen op /api/studenthome/:homeId')

    const homeId = req.params.homeId
    logger.info('Get aangeroepen op /api/studenthome/', homeId)

    sqlDatabase.getStudentHomeById(req.params.homeId, (err, result) => {
      if (err || result.result === 'Studenthome does not exist') {
        res.status(404).json({
          result: 'Studenthome does not exist'
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  },

  changeById(req, res, next) {
    logger.info('Put aangeroepen op /api/studenthome/:homeId')

    let input = req.body

    const updatedStudentHome = {
      ...req.body,
      Id: parseInt(req.params.homeId, 10)
    }

    try {
      const {
        naam,
        straatnaam,
        huisnummer,
        postcode,
        plaats,
        telefoonnummer
      } = req.body

      assert(typeof naam === 'string', 'naam is nodig')
      assert(typeof straatnaam === 'string', 'straatnaam is nodig')
      assert(typeof huisnummer === 'number', 'huisnummer is nodig')
      assert(typeof postcode === 'string', 'postcode is nodig')
      assert.match(
        postcode,
        /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i,
        'foute postcode'
      )
      assert(typeof plaats === 'string', 'plaats is nodig')
      assert(typeof telefoonnummer === 'string', 'verkeerd telefoonnummer')
      assert.match(
        telefoonnummer,
        /(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/,
        'foute telefoonnummer'
      )

      if (req.params.homeId < 0) {
        res.status(400).json({
          message: 'verplicht veld niet ingevuld',
          error: e.toString()
        })
      } else {
        sqlDatabase.changeStudentHome(
          updatedStudentHome.Id,
          req.body,
          req.userId,
          (err, result) => {
            if (err || result.result === 'studenthuis bestaat niet') {
              res.status(404).json({
                result: 'studenthuis bestaat niet'
              })
            } else {
              res.status(200).json({
                result: req.body
              })
            }
          }
        )
      }
    } catch (e) {
      res.status(400).json({
        message: 'verplicht veld niet ingevuld',
        error: e.toString()
      })
    }
  },

  deleteById(req, res, next) {
    logger.debug('Delete aangeroepen op /api/studenthome/:homeId')

    sqlDatabase.deleteStudentHome(
      req.params.homeId,
      req.userId,
      (err, result) => {
        if (err || result.result === 'Studenthome does not exist') {
          res.status(404).json({
            result: 'Studenthome does not exist'
          })
        } else {
          res.status(200).json({
            result: result.studenthome.result
          })
        }
      }
    )
  }
}
module.exports = controller
