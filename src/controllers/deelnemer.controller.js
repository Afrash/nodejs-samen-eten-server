const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const studenthuiscontroller = require('../../src/controllers/studenthuis.controller')
const maaltijdencontroller = require('../../src/controllers/maaltijden.controller')
let database = studenthuiscontroller.database
const sqlDatabase = require('../database/database')

let controller = {
  signupMeal(req, res, next) {
    logger.info('Post aangeroepen op /studenthome/:homeId/meal/:mealId/signup')

    sqlDatabase.signUpForMeal(
      req.params.homeId,
      req.params.mealId,
      (err, result) => {
        if (err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    )
  },

  signoffMeal(req, res, next) {
    logger.info('Put /studenthome/:homeId/meal/:mealId/signoff')

    sqlDatabase.signOffForMeal(
      req.params.homeId,
      req.params.mealId,
      (err, result) => {
        if (err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    )
  },

  getListParticipants(req, res, next) {
    logger.info('Get /meal/:mealId/participants')

    sqlDatabase.getAllParticipants(req.params.mealId, (err, result) => {
      if (err) {
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  },

  getParticipantsById(req, res, next) {
    logger.info('Get /meal/:mealId/participants/:participantId')

    sqlDatabase.getParticipantById(
      req.params.mealId,
      req.params.participantId,
      (err, result) => {
        if (err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    )
  }
}

module.exports = controller
