const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const studenthuiscontroller = require('../../src/controllers/studenthuis.controller')
let database = studenthuiscontroller.database
const sqlDatabase = require('../database/database')

let controller = {
  createMeal(req, res, next) {
    logger.info('Post aangeroepen op /studenthome/:homeId/meal')

    const {
      naam,
      beschrijving,
      aangemaakt,
      aangeboden,
      prijs,
      allergieInformatie,
      ingredienten,
      maxParticipants
    } = req.body

    try {
      assert(typeof naam === 'string', 'naam is niet ingevuld')
      assert(typeof beschrijving === 'string', 'beschrijving is niet ingevuld')
      assert(
        typeof ingredienten === 'string',
        'ingredienten zijn niet ingevuld '
      )
      assert(
        typeof allergieInformatie === 'string',
        'allergie informatie is niet ingevuld'
      )
      assert(typeof aangemaakt === 'string', 'datum is niet ingevuld')
      assert(typeof aangeboden === 'string', 'datum is niet ingevuld')
      assert(typeof prijs === 'number', 'prijs is niet ingevuld')
      assert(
        typeof maxParticipants === 'number',
        'maximale deelnemers is niet ingevoerd '
      )

      sqlDatabase.addMeal(
        req.body,
        req.params.homeId,
        req.userId,
        (err, result) => {
          if (err) {
            res.status(400).json({
              result: 'Studenthome does not exist'
            })
          } else {
            res.status(200).json({
              result: result
            })
          }
        }
      )
    } catch (e) {
      res.status(400).json({
        message: 'Error message',
        error: e.toString()
      })
    }
  },
  updateMeal(req, res, next) {
    logger.info('Put aangeroepen op /studenthome/:homeId/meal/:mealId')

    const {
      homeId,
      mealId,
      naam,
      beschrijving,
      aangemaakt,
      aangeboden,
      prijs,
      allergieInformatie,
      ingredienten,
      maxParticipants
    } = req.body

    try {
      assert(typeof naam === 'string', 'naam is niet ingevuld')
      assert(typeof beschrijving === 'string', 'beschrijving is niet ingevuld')
      assert(typeof aangemaakt === 'string', 'datum is niet ingevuld')
      assert(typeof aangeboden === 'string', 'datum is niet ingevuld')
      assert(typeof prijs === 'number', 'prijs is niet ingevuld')
      assert(
        typeof allergieInformatie === 'string',
        'allergie informatie is niet ingevuld'
      )
      assert(
        typeof ingredienten === 'string',
        'ingredienten zijn niet ingevuld '
      )
      assert(
        typeof maxParticipants === 'number',
        'maximale deelnemers is niet ingevoerd '
      )

      sqlDatabase.updateMeals(
        req.body,
        req.params.homeId,
        req.params.mealId,
        homeid,
        (err, result) => {
          if (err || result.result === 'Meal does not exist') {
            res.status(400).json({
              result: err
            })
          } else {
            res.status(200).json({
              result: req.body
            })
          }
        }
      )
    } catch (e) {
      res.status(400).json({
        message: 'Error message',
        error: e.toString()
      })
    }
  },
  allMeals(req, res, next) {
    logger.info('Get aangeroepen op /studenthome/:homeId/meal')

    sqlDatabase.getAllMeals(req.params.homeId, (err, result) => {
      if (err) {
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  },
  mealById(req, res, next) {
    logger.info('Get aangeroepen op /api/studenthome/:homeId/meal/:mealId')

    sqlDatabase.getMealById(
      req.params.homeId,
      req.params.mealId,
      (err, result) => {
        if (err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    )
  },
  deleteMeal(req, res, next) {
    logger.info('Delete aangeroepen op /api/studenthome/:homeId/meal/:mealId')

    sqlDatabase.deleteMeal(
      req.params.homeId,
      req.params.mealId,
      req.userId,
      (err, result) => {
        if (err || result.result === 'meal does not exist') {
          res.status(404).json({
            result: 'Meal does not exist'
          })
        } else {
          res.status(200).json({
            result: result.meal.result
          })
        }
      }
    )
  }
}
module.exports = controller
