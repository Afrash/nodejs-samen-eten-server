const log = require('../config/config')
const logger = log.logger

let controller = {
  getInfo(req, res) {
    logger.info('Get request op api/info!')

    const info = {
      Studentnaam: 'Afrash Ramjit',
      Studentnummer: '2151957',
      Beschrijving:
        'Dit is een nodeJs server waarmee er kan aangegeven worden welke studenten gaan eten.',
      'Sonarqube Url': ''
    }
    res.status(200).json(info)
  }
}

module.exports = controller
