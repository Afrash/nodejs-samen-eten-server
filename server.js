const express = require('express')
const bodyParser = require('body-parser')
const authroutes = require('./src/routes/authentication.routes')
const studenthuisroutes = require('./src/routes/studenthuis.routes')
const maaltijdenroutes = require('./src/routes/maaltijden.routes')
const deelnemerroutes = require('./src/routes/deelnemer.routes')
const config = require('./src/config/config')
const logger = config.logger
require('dotenv').config();





const app = express()

app.use(bodyParser.json())


const port = process.env.PORT || 3000


app.all('*', (req, res, next) => {
  const method = req.method
  logger.debug('Method', method)
  next()
})

app.use('/api', authroutes)

app.use('/api', studenthuisroutes)

app.use('/api', maaltijdenroutes)

app.use('/api', deelnemerroutes)

//all wrong urls
app.all('*', (req, res, next) => {
  res.status(404).json({
    error: "Endpoint does not exist!"
  })
})

app.listen(port, () =>
  logger.info(`Example app listening at ${port}`))

  module.exports = app